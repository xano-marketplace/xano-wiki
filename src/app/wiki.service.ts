import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';
import {Observable} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class WikiService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public wikiSave(wiki): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/wiki`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: wiki,
		});
	}

	public wikiListGet(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/wiki`,
			params: {search}
		});
	}

	public wikiGet(slug): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/wiki/${slug}`,
		});
	}

	public wikiHistoryGet(wikiId, page = null): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/wiki/${wikiId}/history`,
			params: {external_paging: page}
		});
	}

	public wikiDelete(wikiId): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/wiki/${wikiId}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public wikiUpdate(wiki): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/wiki/${wiki.wiki_id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: wiki
		});
	}
}

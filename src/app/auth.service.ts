import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public login(login: { email: string, password: string }): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/login`,
			params: {
				email: login.email,
				password: login.password
			}
		});
	}

	public signup(signup: { name: string, email: string, password: string }): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/signup`,
			params: {
				name: signup.name,
				email: signup.email,
				password: signup.password
			}
		});
	}
}

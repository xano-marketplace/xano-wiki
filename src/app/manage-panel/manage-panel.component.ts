import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {WikiService} from '../wiki.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import {finalize, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {get} from 'lodash-es';

export interface Category {
	id: number;
	category_id: number;
	category: string;
}

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {
	public config: XanoConfig;
	readonly separatorKeysCodes: number[] = [ENTER, COMMA];
	public deleting: boolean = false;
	public saving: boolean = false;
	public uploading: boolean = false;
	public image: any;

	public wikiForm: FormGroup = new FormGroup({
		wiki_id: new FormControl(null),
		title: new FormControl('', Validators.required),
		description: new FormControl('', Validators.required),
		content: new FormControl('', Validators.required),
		slug: new FormControl('', Validators.required)
	});

	public tags = new Set();

	public modules = {
		toolbar: [
			['bold', 'italic', 'underline'],
			['blockquote'],
			[{header: 1}, {header: 2}],
			[{list: 'ordered'}, {list: 'bullet'}],
			[{color: []}],
			['link']
		]
	};


	constructor(
		private configService: ConfigService,
		private wikiService: WikiService,
		private dialogRef: MatDialogRef<ManagePanelComponent>,
		@Inject(MAT_DIALOG_DATA) public data,
		private snackBar: MatSnackBar) {
	}


	ngOnInit(): void {
		this.config = this.configService.config;

		if (this.data?.wiki) {
			this.wikiForm.patchValue(this.data.wiki);
			this.wikiForm.controls.wiki_id.patchValue(this.data.wiki.id);
			this.tags = new Set(this.data.wiki.tags);
		}
		this.wikiForm.controls.title.valueChanges.subscribe(value => {
			if (!this.wikiForm.controls.slug.dirty) {
				this.wikiForm.controls.slug.patchValue(value.replace(/\s+/g, '-').toLowerCase());
			}
		});
	}

	public submit(): void {
		this.wikiForm.markAllAsTouched();
		if (this.wikiForm.valid) {
			if (!this.data?.wiki) {
				this.wikiService.wikiSave({
					...this.wikiForm.value,
					tags: [...this.tags],
				}).pipe(
					tap(x => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({new: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			} else {
				this.wikiService.wikiUpdate({
					...this.wikiForm.value,
					tags: [...this.tags],
				}).pipe(
					tap(() => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({updated: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			}
		} else {
			this.snackBar.open('Please Fix Form', 'Error', {panelClass: 'error-snack'});
		}
	}

	delete() {
		this.wikiService.wikiDelete(this.data.wiki.id)
			.pipe(
				tap(() => this.deleting = true),
				finalize(() => this.deleting = false)
			)
			.subscribe((res: any) => {
				this.dialogRef.close({deleted: true, item: {id: this.data.wiki.id}});
			}, err => {
				this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
			});
	}

	addTag(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		if ((value || '').trim()) {
			this.tags.add(value.trim());
		}
		if (input) {
			input.value = '';
		}
	}

	removeTag(tag): void {
		this.tags.delete(tag);
	}
}

import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../auth.service';
import {MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
	selector: 'app-auth-panel',
	templateUrl: './auth-panel.component.html',
	styleUrls: ['./auth-panel.component.scss']
})
export class AuthPanelComponent implements OnInit {
	public config: XanoConfig;
	public showLoginView: boolean = true;
	private emailRegex = new RegExp('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$');

	public loginForm: FormGroup = new FormGroup({
		email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex)]),
		password: new FormControl('', Validators.required)
	});
	public signupForm: FormGroup = new FormGroup({
		name: new FormControl('', [Validators.required]),
		email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex)]),
		password: new FormControl('', Validators.required)
	});

	constructor(
		private configService: ConfigService,
		private authService: AuthService,
		private dialogRef: MatDialogRef<AuthPanelComponent>,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
	}

	public submit(): void {
		if (this.showLoginView) {
			this.loginForm.markAllAsTouched();

			if (this.loginForm.valid) {
				this.authService.login(this.loginForm.getRawValue()).subscribe(authToken => {
					if (authToken) {
						this.configService.authToken.next(authToken.authToken);
						this.dialogRef.close();
						this.dialogRef.close();
					} else {
						this.snackBar.open('No Auth Token', 'Error', {panelClass: 'error-snack'});
					}
				}, err => {
					this.snackBar.open(err.error.message, 'Error', {panelClass: 'error-snack'});
				});
			}
		} else {
			if (this.signupForm.valid) {
				this.authService.signup(this.signupForm.getRawValue()).subscribe(authToken => {
					if (authToken) {
						this.configService.authToken.next(authToken.authToken);
						this.dialogRef.close();
					} else {
						this.snackBar.open('No Auth Token', 'Error', {panelClass: 'error-snack'});
					}
				}, err => {
					this.snackBar.open(err.error.message, 'Error', {panelClass: 'error-snack'});
				});
			}
		}
	}

}

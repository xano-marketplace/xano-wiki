import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter, map, mergeMap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ConfigPanelComponent} from '../config-panel/config-panel.component';
import {AuthPanelComponent} from '../auth-panel/auth-panel.component';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';

@Component({
	selector: 'app-action-bar',
	templateUrl: './action-bar.component.html',
	styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {

	public isLoggedIn: boolean = false;
	public isConfigured: boolean = false;
	public config: XanoConfig;
	public isHome: boolean = true;
	public user: any;

	constructor(
		private configService: ConfigService,
		private router: Router,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res => this.isConfigured = res);
		this.config = this.configService.config;

		this.configService.isLoggedIn()
			.pipe(mergeMap(loggedIn => {
				this.isLoggedIn = loggedIn;
				if (loggedIn) {
					return this.configService.user.asObservable();
				} else {
					return EMPTY;
				}
			}))
			.subscribe(user => {
				this.user = user;
			});

		this.router.events.pipe(
			filter((event: RouterEvent) => event instanceof NavigationEnd),
			map(event => event.url)
		).subscribe(url => this.isHome = (url === '/' || url.includes('tag')));

	}

	public logout(): void {
		this.configService.authToken.next(null);
		this.configService.user.next(null);
	}

	public showPanel(dialogType): void {
		let dialogRef;
		switch (dialogType) {
			case 'config':
				dialogRef = this.dialog.open(ConfigPanelComponent);
				break;
			case 'auth':
				dialogRef = this.dialog.open(AuthPanelComponent);
				break;
			case 'manage':
				dialogRef = this.dialog.open(ManagePanelComponent, {minWidth: '50vw'});
				dialogRef.afterClosed().subscribe(res => {
					if (res) {
						this.configService.newWiki.next(res);
					}
				});
				break;
			default:
				break;
		}
	}

}

import {Component} from '@angular/core';
import {ConfigService} from './config.service';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html'
})
export class AppComponent {
	public appSummary: string = '';

	constructor(configService: ConfigService, public title: Title, router: Router) {
		title.setTitle(configService.config.title);
		this.appSummary = configService.config.summary;

		setTimeout(() => {
			configService.isConfigured().subscribe(res => {
				if (!res) {
					router.navigate(['']);
				}
			});
		},1000)

	}
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {WikiViewComponent} from './wiki-view/wiki-view.component';

const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'tag/:tag', component: HomeComponent},
	{path: 'wiki/:slug', component: WikiViewComponent},
	{path: '**', redirectTo: ''}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}

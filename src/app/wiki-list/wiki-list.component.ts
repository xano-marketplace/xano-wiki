import {Component, OnInit} from '@angular/core';
import {WikiService} from '../wiki.service';
import {debounceTime, distinctUntilChanged, finalize, tap} from 'rxjs/operators';
import * as _ from 'lodash-es';
import {ConfigService} from '../config.service';
import {FormControl} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute} from '@angular/router';

export interface Wiki {
	id: number;
	created_at: number;
	title: string;
	slug: string;
	content: string;
	description: string;
	tags: string[];
}

@Component({
	selector: 'app-wiki-list',
	templateUrl: './wiki-list.component.html',
	styleUrls: ['./wiki-list.component.scss']
})
export class WikiListComponent implements OnInit {
	public wikis: Wiki[] = [];
	public form: any;
	public search: FormControl = new FormControl('');
	public loading: boolean = true;
	public loggedIn: boolean = false;
	public currentPage: number;
	public totalItems: number;

	constructor(
		private wikiService: WikiService,
		private configService: ConfigService,
		private snackBar: MatSnackBar,
		private route: ActivatedRoute
	) {
	}

	ngOnInit(): void {
		this.configService.isLoggedIn().subscribe(loggedIn => this.loggedIn = loggedIn);

		this.configService.newWiki.asObservable().subscribe(res => {
			if (res?.new) {
				this.wikis.unshift(res.item);
			}
		});

		this.search.valueChanges
			.pipe(
				debounceTime(300),
				distinctUntilChanged(),
			)
			.subscribe(() => {
				this.getWikiList();
			});

		if (this.route.snapshot.params.tag) {
			this.search.patchValue(this.route.snapshot.params.tag);
		}
		this.getWikiList();
	}

	public getWikiList(page = 1): void {
		this.loading = true;
		const search = {
			page: page,
			expression: []
		};

		const searchValue = this.search.value.trim();

		if (searchValue) {
			search.expression.push({
				'type': 'group',
				'group': {
					'expression': [
						{
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.title'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.description'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.content'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						},
						{
							'or': true,
							'statement': {
								'left': {
									'tag': 'col',
									'operand': 'wiki.tags'
								},
								'op': 'includes',
								'right': {
									'operand': `${searchValue}`
								}
							}
						}
					]
				}
			});
		}
		this.wikiService.wikiListGet(search).pipe(
			tap(() => this.loading = true),
			finalize(() => this.loading = false)
		).subscribe(res => {
			this.wikis = res.items;
			this.totalItems = res.itemsTotal;
			this.currentPage = res.curPage;
			if (!res.length) {
				this.loading = false;
			}
		}, (err) => {
			this.loading = false;
			this.snackBar.open(_.get(err, 'error.message', 'An unknown error has occurred.'), 'Error', {panelClass: 'error-snack'});
		});
	}

	public clearSearch() {
		this.search.patchValue('');
	}

	public pageChangeEvent(event): void {
		this.getWikiList(event.pageIndex + 1);
	}
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {WikiListComponent} from './wiki-list/wiki-list.component';
import {WikiViewComponent} from './wiki-view/wiki-view.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {QuillModule} from 'ngx-quill';
import {AuthPanelComponent} from './auth-panel/auth-panel.component';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCommonModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {HeaderComponent} from './header/header.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {SafeHtmlPipe} from './safe-html.pipe';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ManagePanelComponent} from './manage-panel/manage-panel.component';
import {MatChipsModule} from '@angular/material/chips';
import {CommonModule} from '@angular/common';
import {MatTableModule} from '@angular/material/table';

@NgModule({
	declarations: [
		AppComponent,
		ActionBarComponent,
		HomeComponent,
		ConfigPanelComponent,
		ManagePanelComponent,
		HeaderComponent,
		WikiListComponent,
		WikiViewComponent,
		AuthPanelComponent,
		SafeHtmlPipe
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		CommonModule,
		MatCommonModule,
		AppRoutingModule,
		HttpClientModule,
		QuillModule.forRoot(),
		ReactiveFormsModule,
		MatDialogModule,
		MatSnackBarModule,
		MatCardModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule,
		MatIconModule,
		MatProgressSpinnerModule,
		MatPaginatorModule,
		MatChipsModule,
		MatTableModule
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		}
	}
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}

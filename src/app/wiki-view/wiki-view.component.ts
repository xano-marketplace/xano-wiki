import {Component, EventEmitter, OnInit} from '@angular/core';
import {WikiService} from '../wiki.service';
import {ActivatedRoute, Router} from '@angular/router';
import {concatMap, finalize} from 'rxjs/operators';
import {ConfigService} from '../config.service';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';
import {MatDialog} from '@angular/material/dialog';
import {AuthPanelComponent} from '../auth-panel/auth-panel.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
	selector: 'app-wiki-view',
	templateUrl: './wiki-view.component.html',
	styleUrls: ['./wiki-view.component.scss']
})
export class WikiViewComponent implements OnInit {
	public wiki: any;
	public latestWiki: any;
	public loggedIn: boolean = false;
	public loading: boolean = true;
	public showHistory: boolean = false;
	public wikiHistory: any = [];
	public displayedColumns : string[] = ['id', 'edited_by', 'edit_date'];

	constructor(
		private wikiService: WikiService,
		private route: ActivatedRoute,
		private router: Router,
		private configService: ConfigService,
		private dialog: MatDialog,
		private snackBar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.configService.isLoggedIn().subscribe(loggedIn => this.loggedIn = loggedIn);
		this.route.params.pipe(
			concatMap(params => this.wikiService.wikiGet(params.slug).pipe(finalize(() => this.loading = false))
		)).subscribe(wiki => {
			this.latestWiki = wiki;
			this.wiki = wiki;
		});
	}

	public addWiki(): void {
		const dialogRef = this.dialog.open(ManagePanelComponent);
		dialogRef.afterClosed().subscribe(res => {
			if (res?.new) {
				this.snackBar.open('Wiki', 'Added');
				this.router.navigate(['wiki', res.item.slug]);
			}
		});
	}

	public editWiki() {
		const dialogRef = this.dialog.open(ManagePanelComponent, {minWidth: '50vw', data: {wiki: this.latestWiki}});

		dialogRef.afterClosed().subscribe(res => {
			if (res?.updated) {
				this.wiki = res.item;
				this.snackBar.open('Wiki', 'Updated');
			} else if(res?.deleted) {
				this.snackBar.open('Wiki', 'Deleted')
				this.router.navigate(['']);
			}
		});
	}

	public showHistoryFn(wikiId, page = null) {
		this.showHistory = true;
		if (this.showHistory) {
			this.wikiService.wikiHistoryGet(wikiId, page).subscribe(res => this.wikiHistory = res);
		}
	}

	public viewVersion(wiki) {
		this.wiki = wiki;
		this.showHistory = false;
	}

	public pageChangeEvent(event): void {
		this.showHistoryFn(this.latestWiki.id, {page: event.pageIndex + 1});
	}
}

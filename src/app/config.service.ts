import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {XanoService} from './xano.service';
import {ApiService} from './api.service';

export interface XanoConfig {
	title: string;
	summary: string;
	editText: string;
	editLink: string;
	descriptionHtml: string;
	logoHtml: string;
	requiredApiPaths: string[];
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public newWiki: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public config: XanoConfig = {
		title: 'Xano Wiki',
		summary: 'This extension provides a fully functioning demo to manage a wiki.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-wiki',
		descriptionHtml: `
                <h2>Description</h2>
                <p>This demo consists of following components:</p>
                <h2>Personal View</h2>
                <p>
                	This is your wiki where you see a list of posted wikis. You are able to read wikis by clicking cards in the list.
                 	You are also able to post new wikis, as well as edit or delete existing wikis once logged in.
                 </p>
                 <p>
                 	Additionally, an example of version history of the wiki is included.
				</p>
				<h2>Authenticated View</h2>
				<p>
					This extension requires authentication to add, update, or delete wikis. Once logged in you will see buttons for adding a 
					wiki or modifying an existing wiki.
				</p>
                <h2>Manage View</h2>
                <p>This is a side panel that lets you add a new wiki or edit an existing wiki. The edit component also supports deleting a wiki.</p>
                <h2>Search</h2>
                <p>A search box is present to allow you to search the title, description, content, as well as tags of a wiki.</p>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/wiki',
			'/wiki/{wiki_id}/history',
			'/wiki/{slug}',
			'/auth/login',
			'/auth/signup'
		]
	};

	constructor(private http: HttpClient, private xanoService: XanoService, private apiService: ApiService) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}
}

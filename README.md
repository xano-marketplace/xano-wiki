# Xano Wiki

This is a wiki demo that has been generated from the Xano Boilerplate.

## Core Structure
The folder structure is intended to be clean as possible. Specs have been removed, and majority
of site components that are used throughout the application are contained in the `/app/shared` folder.

### Setup & Dependencies
This boilerplate is intended to be as light as possible so outside of the core 
Angular packages only the following are included:

#### List of Dependencies
* material
* bootstrap
* lodash-es
* ngx-quill

#### Setup

You can use either npm or yarn to install dependencies.

run `npm install` or `yarn`

#### Features 

###### Panels
For any modification of data please use the panels. For an example please see the config-panel component 
`/app/shared/config-panel`.

######  Forms and Inputs
For forms and inputs there is a form-generator included. For an example please see the config-panel component 
`/app/shared/config-panel`.


###### API request
There is an api.service file included, this should be imported into component and/or feature services.

###### Routing
Standard Angular Routing is included in this boilerplate


###### Adding New Features
For a given demo you may need x,y,z features so add as you'd like with standard angular practices



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Spec files have been removed.


## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
